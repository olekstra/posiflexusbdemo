using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace PosiflexUsbDemo {
	public class MainForm : Form {
		private System.Windows.Forms.Button OpenButton;
		private System.Windows.Forms.Button CloseButton;
		private System.Windows.Forms.Button StatusButton;
		private System.Windows.Forms.Button SendButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox SendBox;
		private System.Windows.Forms.TextBox LogBox;

		private Container components = null;

		public MainForm() {
			InitializeComponent();
		}

		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows MainForm Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.OpenButton = new System.Windows.Forms.Button();
			this.CloseButton = new System.Windows.Forms.Button();
			this.StatusButton = new System.Windows.Forms.Button();
			this.SendButton = new System.Windows.Forms.Button();
			this.SendBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.LogBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// OpenButton
			// 
			this.OpenButton.Location = new System.Drawing.Point(24, 16);
			this.OpenButton.Name = "OpenButton";
			this.OpenButton.Size = new System.Drawing.Size(80, 24);
			this.OpenButton.TabIndex = 0;
			this.OpenButton.Text = "Open USB";
			this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
			// 
			// CloseButton
			// 
			this.CloseButton.Location = new System.Drawing.Point(120, 16);
			this.CloseButton.Name = "CloseButton";
			this.CloseButton.Size = new System.Drawing.Size(80, 24);
			this.CloseButton.TabIndex = 0;
			this.CloseButton.Text = "Close USB";
			this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
			// 
			// StatusButton
			// 
			this.StatusButton.Location = new System.Drawing.Point(216, 16);
			this.StatusButton.Name = "StatusButton";
			this.StatusButton.Size = new System.Drawing.Size(80, 24);
			this.StatusButton.TabIndex = 0;
			this.StatusButton.Text = "Status?";
			this.StatusButton.Click += new System.EventHandler(this.StatusButton_Click);
			// 
			// SendButton
			// 
			this.SendButton.Location = new System.Drawing.Point(216, 80);
			this.SendButton.Name = "SendButton";
			this.SendButton.Size = new System.Drawing.Size(80, 24);
			this.SendButton.TabIndex = 0;
			this.SendButton.Text = "Send";
			this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
			// 
			// SendBox
			// 
			this.SendBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.SendBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.SendBox.Location = new System.Drawing.Point(24, 72);
			this.SendBox.Multiline = true;
			this.SendBox.Name = "SendBox";
			this.SendBox.Size = new System.Drawing.Size(184, 48);
			this.SendBox.TabIndex = 1;
			this.SendBox.Text = "������! 100.00";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.label2.Location = new System.Drawing.Point(24, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(160, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "12345678901234567890";
			// 
			// LogBox
			// 
			this.LogBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.LogBox.Location = new System.Drawing.Point(8, 152);
			this.LogBox.Multiline = true;
			this.LogBox.Name = "LogBox";
			this.LogBox.ReadOnly = true;
			this.LogBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.LogBox.Size = new System.Drawing.Size(328, 128);
			this.LogBox.TabIndex = 3;
			this.LogBox.Text = "log window";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(344, 285);
			this.Controls.Add(this.LogBox);
			this.Controls.Add(this.SendBox);
			this.Controls.Add(this.OpenButton);
			this.Controls.Add(this.CloseButton);
			this.Controls.Add(this.StatusButton);
			this.Controls.Add(this.SendButton);
			this.Controls.Add(this.label2);
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.Run(new MainForm());
		}
		
		[DllImport("usbpd.dll")]
		public static extern int OpenUSBpd();
		
		[DllImport("usbpd.dll")]
		public static extern int CloseUSBpd();
		
		[DllImport("usbpd.dll")]
		public static extern int PdState();
		
		[DllImport("usbpd.dll")]
		public static extern int WritePD(byte[] data, int length) ;
//		public static extern int WritePD(string data, int length);

		private void LogAction(string text, long result) {
			LogBox.AppendText(text + ": " + result + "\n");
			LogBox.ScrollToCaret();
		}
		
		private void OpenButton_Click(object sender, System.EventArgs e) {
			LogAction("Open", OpenUSBpd());
			byte[] x = new byte[3];
			x[0] = 0x1B; // 27, ESC
			x[1] = 0x74; // t
			x[2] = 0x06; // who knows :(
			LogAction("Set charset:", WritePD(x, 3));
			//string s = "\x1Bt\x0E"; // ESC t 14
			//LogAction("Set charset:", WritePD(s, s.Length));
		}

		private void CloseButton_Click(object sender, System.EventArgs e) {
			LogAction("Close", CloseUSBpd());
		}

		private void StatusButton_Click(object sender, System.EventArgs e) {
			LogAction("Status", PdState());
		}

		private void SendButton_Click(object sender, System.EventArgs e) {
			ResetPosition();
			string s = this.SendBox.Text;
			if (s.Length > 40) {
				s = s.Substring(0, 40);
			}
			//s = s.PadRight(40);
			byte[] inn = Encoding.Default.GetBytes(s);
			byte[] oout = Encoding.Convert(Encoding.Default, Encoding.GetEncoding(866), inn);
			LogAction("Sending text", WritePD(oout, oout.Length));
			//s = Encoding.GetEncoding(866).GetString(oout);
			//LogAction("Sending text", WritePD(s, s.Length));
		}
		
		private void ResetPosition() {
			byte[] x = new byte[1];
			x[0] = 0x0C; // FormFeed
			LogAction("Reset position(?):", WritePD(x, 0));

		}
	}
}